## About this Project

This project is face emotion detection which uses Haar cascade to detect the face and Keras to detect the emotion.

Training is metioned in the model and the trained model over dataset fer2013 is stored in Emotiondetectionmodel.h5

Default haar cascade face detector is used.

Run the main program to detect the emotion.
